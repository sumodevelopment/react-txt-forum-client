# React TXT Forum
A simple React application using Redux to demonstration the features. 

# Components/Features

```
└── src/components/
    └── ...
```

### Login
  Login a user using username and password
### Navbar
  Global navbar that can access the profile page
### PostCreate
  Create a new post
### Posts
  Timeline to display available posts
### Profile
  Display user profile, logout and posts made by user
### Register
  Create a new account and choose an avatar

# React Redux
## Actions
```
└── src/store/actions
    ├── loginActions.js                   
    ├── postsActions.js                   
    ├── profileActions.js                 
    ├── registerActions.js                       
    └── sessionActions.js
```
 
## Middleware
```
└── src/store/middleware
    ├── appMiddleware.js                   
    ├── loginMiddleware.js                   
    ├── postsMiddleware.js                 
    ├── profilePostsMiddleware.js            
    ├── registerMiddleware.js                
    └── sessionMiddleware.js
```

## Reducers
```
└── src/store/reducers
    ├── loginReducer.js                   
    ├── postsReducer.js                   
    ├── profileReducer.js                 
    ├── profilePostsReducer.js            
    ├── registerReducer.js                
    └── sessionReducer.js
```



## Styling 
The application is using Bootstrap 5.x

**Source:** [https://getbootstrap.com](https://getbootstrap.com)