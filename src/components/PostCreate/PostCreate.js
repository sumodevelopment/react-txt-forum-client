import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import Swal from "sweetalert2"
import withReactContent from "sweetalert2-react-content"
import { postsCreateAction } from "../../store/actions/postsActions"

const PostCreate = () => {

    const { id: userId } = useSelector(state => state.session)
    const dispatch = useDispatch()
    const mySwal = withReactContent(Swal)

    const [post, setPost] = useState({
        title: '',
        body: '',
        postError: ''
    })

    const onPostSubmit = e => {
        e.preventDefault()

        if (!post.title || !post.body) {
            mySwal.fire({
                backdrop: true,
                title: <p>Missed something</p>,
                html: '<span class="material-icons">error</span><br>Please make sure you add a <b>title</b> and a <b>body</b> for the post 👌',
                confirmButtonText: 'Gotcha'
            }).then(_ => {})
            return;
        }

        dispatch(postsCreateAction({ ...post, userId }))
        setPost({
            title: '',
            body: '',
            postError: ''
        })
    }

    const onInputChange = e => {
        setPost({
            ...post,
            [e.target.id]: e.target.value
        })
    }

    return (
        <article className="card mb-4">
            <section className="card-header pb-0">
                <h4 className="card-title">Post something new</h4>
                <p className="card-sub-title">What's on your mind?</p>
            </section>
            <section className="card-body">
                <form onSubmit={onPostSubmit}>
                    <div className="mb-3">
                        <label htmlFor="title" className="form-label visually-hidden" aria-label="Post title">Post title</label>
                        <input id="title" onChange={ onInputChange } type="text" 
                                        className="form-control" 
                                        placeholder="Post title" 
                                        value={ post.title }
                                        />
                    </div>

                    <div className="mb-3">
                        <label htmlFor="body" className="form-label visually-hidden" aria-label="Post body">Post body</label>
                        <textarea id="body" onChange={ onInputChange } 
                                    rows="2" 
                                    className="form-control" 
                                    placeholder="Post body" 
                                    value={ post.body }
                                    ></textarea>
                    </div>

                    <button className="btn btn-warning d-flex" type="submit">
                        <span className="material-icons">create</span> &nbsp;
                        <span>Post</span>
                    </button>
                </form>
            </section>
        </article>
    )
}
export default PostCreate