import { DateUtil } from "../../util/date.util"

const Post = ({post}) => {
    return (
        <article className="card mb-4">
            <section className="card-body">
                <h4 className="card-title">{post.title}</h4>
                <p className="card-text">{post.body}</p>
            </section>
            <section className="card-footer">
                by: {post.username} { DateUtil.formatForDisplay(post.createdAt) }
            </section>
        </article>
    )
}
export default Post