import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import AppContainer from '../../hoc/AppContainer'
import { postsFetchingAction } from "../../store/actions/postsActions"
import PostCreate from "../PostCreate/PostCreate"
import Post from "./Post"
const Posts = () => {

    const {
        posts,
        postsFetching,
        postsError
    } = useSelector(state => state.posts)

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(postsFetchingAction())
    }, [dispatch])

    return (
        <AppContainer>
            <header>
                <PostCreate />
            </header>
            {postsFetching && <p>Getting the latest posts...</p>}
            {postsError && <p>{postsError}</p>}
            <section>
                {posts.map(post => <Post key={post.id} post={post} />)}
            </section>
        </AppContainer>
    )
}
export default Posts