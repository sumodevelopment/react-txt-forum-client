import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { profilePostsFetchingAction } from "../../store/actions/profilePostsActions"
import Post from "../Posts/Post"

const ProfilePosts = () => {

    const { profilePosts } = useSelector(state => state.profilePosts)
    const session = useSelector(state => state.session)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(profilePostsFetchingAction(session.id))
    }, [dispatch, session])

    return (
        <>
            <h4 className="mb-3">Checkout your posts</h4>
            <section>
                {profilePosts.map(post => <Post key={post.id} post={post} />)}
            </section>
        </>

    )
}
export default ProfilePosts