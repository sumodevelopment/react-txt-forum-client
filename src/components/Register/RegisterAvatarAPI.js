export const RegisterAvatarAPI = {
    getAvatars() {
        return fetch('https://noroff-react-txt-forum-api.herokuapp.com/avatars')
            .then(async response => {
                if (!response.ok) {
                    const { error = 'An unknown error occurred' } = await response.json()
                    throw new Error(error)
                }
                return response.json()
            })
    }
}