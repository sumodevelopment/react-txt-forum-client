import styles from './RegisterAvatars.module.css'

const RegisterAvatarImage = ({ avatar, click, selected }) => {

    const className = [styles.RegisterAvatarImage]

    if (selected) {
        className.push(styles.RegisterAvatarImageSelected)
    }

    return (
        <figure className={ className.join(' ') } onClick={ () => click(avatar) }>
            <img className="img-fluid" src={avatar.url} alt={avatar.url} />
        </figure>
    )
}
export default RegisterAvatarImage