import { useEffect, useState } from "react"
import { RegisterAvatarAPI } from "./RegisterAvatarAPI";
import RegisterAvatarImage from "./RegisterAvatarImage";
import styles from './RegisterAvatars.module.css'

const RegisterAvatars = ({ onAvatarChange }) => {

    const [state, setState] = useState({
        avatars: [],
        fetchingAvatars: true,
        selectedAvatar: -1,
        avatarError: ''
    });

    useEffect(() => {
        RegisterAvatarAPI.getAvatars()
            .then(avatars => {
                setState({
                    ...state,
                    fetchingAvatars: false,
                    avatars
                })
            })
            .catch(({ message }) => {
                setState({
                    ...state,
                    fetchingAvatars: false,
                    avatarError: message
                })
            });
    }, [])

    const onAvatarClick = avatar => {
        setState({
            ...state,
            selectedAvatar: avatar
        })
        onAvatarChange(avatar)
    }
    
    return (
        <section>
            <h4>Choose your avatar</h4>
            { state.fetchingAvatars && <p>Loading avatars...</p>}
            <div className={styles.RegisterAvatars}>
                {state.avatars.map(avatar => {
                    return (
                        <RegisterAvatarImage 
                                click={ onAvatarClick } 
                                avatar={avatar} 
                                key={avatar.id} 
                                selected={ avatar.id === state.selectedAvatar.id }
                                />
                    )
                })}
            </div>

        </section>
    )
}
export default RegisterAvatars