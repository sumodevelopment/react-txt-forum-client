export const ACTION_POSTS_FETCHING = '[posts] FETCHING'
export const ACTION_POSTS_SET = '[posts] SET'
export const ACTION_POSTS_FETCH_ERROR = '[posts] FETCH_ERROR'
export const ACTION_POSTS_CREATE = '[posts] CREATE'
export const ACTION_POSTS_ADD = '[posts] ADD'
export const ACTION_POSTS_CREATE_ERROR = '[posts] CREATE_ERROR'

export const postsFetchingAction = () => ({
    type: ACTION_POSTS_FETCHING
})

export const postsSetAction = posts => ({
    type: ACTION_POSTS_SET,
    payload: posts
})

export const postsFetchErrorAction = error => ({
    type: ACTION_POSTS_FETCH_ERROR,
    payload: error
})

export const postsCreateAction = post => ({
    type: ACTION_POSTS_CREATE,
    payload: post
})

export const postsAddAction = post => ({
    type: ACTION_POSTS_ADD,
    payload: post
})


export const postsCreateErrorAction = error => ({
    type: ACTION_POSTS_CREATE_ERROR,
    payload: error
})