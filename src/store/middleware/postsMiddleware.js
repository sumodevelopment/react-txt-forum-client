import { PostCreateAPI } from "../../components/PostCreate/PostCreateAPI"
import { PostsAPI } from "../../components/Posts/PostsAPI"
import { API_ERROR_INVALID_AUTH } from "../../util/api.util"
import { ACTION_POSTS_CREATE, ACTION_POSTS_FETCHING, postsAddAction, postsCreateErrorAction, postsFetchErrorAction, postsSetAction } from "../actions/postsActions"
import { sessionExpiredAction } from "../actions/sessionActions"

export const postsMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if (action.type === ACTION_POSTS_FETCHING) {

        PostsAPI.getPosts()
            .then(posts => {
                dispatch(postsSetAction(posts))
            })
            .catch(({ message }) => {
                if (message === API_ERROR_INVALID_AUTH) {
                    dispatch(sessionExpiredAction())
                } else {
                    dispatch(postsFetchErrorAction(message))
                }
            })
    }

    if (action.type === ACTION_POSTS_CREATE) {

        PostCreateAPI.createPost(action.payload)
            .then(post => {
                dispatch(postsAddAction(post))
            })
            .catch(({ message }) => {
                if (message === API_ERROR_INVALID_AUTH) {
                    dispatch(sessionExpiredAction())
                } else {
                    dispatch(postsCreateErrorAction(message))
                }
            })

    }

}