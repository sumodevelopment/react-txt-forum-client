import { ACTION_POSTS_ADD, ACTION_POSTS_CREATE_ERROR, ACTION_POSTS_FETCHING, ACTION_POSTS_FETCH_ERROR, ACTION_POSTS_SET } from "../actions/postsActions";

const initialState = {
    posts: [],
    postsFetching: false,
    postsError: '',
    postsCreateError: ''
}

export const postsReducer = (state = { ...initialState }, action) => {
    switch (action.type) {

        case ACTION_POSTS_FETCHING:
            return {
                ...state,
                postsFetching: true,
                postsError: ''
            }
        case ACTION_POSTS_SET:
            return {
                ...state,
                postsFetching: false,
                postsError: '',
                posts: [...action.payload]
            }
        case ACTION_POSTS_FETCH_ERROR:
            return {
                ...state,
                postsFetching: false,
                postsError: action.payload
            }
        case ACTION_POSTS_ADD: 
            return {
                ...state,
                posts: [action.payload, ...state.posts]
            }
        case ACTION_POSTS_CREATE_ERROR:
            return {
                ...state,
                postsCreateError: action.payload
            }
        default:
            return state

    }
}